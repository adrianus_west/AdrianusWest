<!--
**AdrianusWest/AdrianusWest** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

![](http://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=AdrianusWest&theme=nord_dark)
![](http://github-profile-summary-cards.vercel.app/api/cards/repos-per-language?username=AdrianusWest&theme=nord_dark)
![](http://github-profile-summary-cards.vercel.app/api/cards/most-commit-language?username=AdrianusWest&theme=nord_dark)
![](http://github-profile-summary-cards.vercel.app/api/cards/stats?username=AdrianusWest&theme=nord_dark)
![](http://github-profile-summary-cards.vercel.app/api/cards/productive-time?username=AdrianusWest&theme=nord_dark&utcOffset=3)

---

### Здравствуй, гость! ![Посетители](https://visitor-badge.glitch.me/badge?page_id=AdrianusWest)

- 🔭 В настоящее время я работаю над проектами, указанными в моих репозиториях. 
- 🌱 Я сейчас изучаю Python. Вторым языком рассматриваю для себя Rust. Разумеется, параллельно с языками я учу так же сопутствующие современные инструменты разработчика, которые могут мне понадобиться либо являются интересными.
- 💬 Все вопросы можно задать в личных сообщениях или письмах.
- 📫 Как со мной связаться:

*Telegram: @adrian_carver*

*E-mail: adrian.carver@yandex.ru*

---

### Welcome, guest! ![Visitors](https://visitor-badge.glitch.me/badge?page_id=AdrianusWest) 

- 🔭 I'm currently working on the projects posted in my repositories. 
- 🌱 I'm currently learning Python. Second language I'm considering Rust for myself. Of course, in parallel with the languages, I'm also learning the related modern developer tools that I might need or be interested in.
- 💬 All questions can be asked in private messages or emails.
- 📫 How to contact me:

*Telegram: @adrian_carver*

*E-mail: adrian.carver@yandex.ru*

---